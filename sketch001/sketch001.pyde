linePosition = []
linePositionCursor = 0

def setup():
    size(500, 500)
    background(255)

def stopDraw(linePositionCursor):
    global linePosition
    
    invertedLinePosition = linePosition[::-1]

    stroke(255)
    strokeWeight(3)
    line(invertedLinePosition[linePositionCursor], 0, invertedLinePosition[linePositionCursor], height)

def startDraw():
    global linePosition
    
    strokeWeight(3)
    
    lineX = random(height)
    r = random(255)
    g = random(255)
    b = random(255)
    stroke(r, g, b)
    line(lineX, 0, lineX, height)
    
    linePosition.append(lineX)
    
    
def draw():
    global linePosition, linePositionCursor
    
    if frameCount < 100:
        startDraw()
    
    if frameCount >= 100:    
        stopDraw(linePositionCursor)
        
        if linePositionCursor == len(linePosition) - 1:
            noLoop()
            
        linePositionCursor += 1
        